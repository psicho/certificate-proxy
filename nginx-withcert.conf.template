user  nginx;
worker_processes  1;

pid        /var/run/nginx.pid;
error_log  /var/log/nginx/error.log warn;

events {
    worker_connections  1024;
}

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    keepalive_timeout   65;
    gzip                on;
    server_tokens       off;

    proxy_buffering     off;
    proxy_redirect      off;
    proxy_set_header    Host $host;
    proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header    X-Forwarded-Ssl on;

    ssl_protocols               TLSv1.2 TLSv1.3;
    ssl_ciphers                 HIGH:!aNULL:!MD5;
    ssl_prefer_server_ciphers   on;

    server_names_hash_bucket_size 128;
    client_max_body_size          512M;

    server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name _;
        return 301 https://$host$request_uri;
    }

    % for domainMapping in domainMappings:
    server {
        listen                  ${domainMapping['listeningPort']} ssl;
        server_name             ${domainMapping['domain']};
        ssl_certificate         /etc/letsencrypt/live/${domainMapping['certificate']}/fullchain.pem;
        ssl_certificate_key     /etc/letsencrypt/live/${domainMapping['certificate']}/privkey.pem;
        location ~ ^/?(.*)$ {
            resolver            127.0.0.11;
            proxy_pass          ${domainMapping['target']};
            proxy_redirect      http:// https://;
            proxy_set_header    Host $host:$server_port;
            proxy_set_header    Upgrade $http_upgrade;
            proxy_set_header    Connection "upgrade";
            proxy_set_header    X-Forwarded-Host $host:$server_port;
            proxy_set_header    X-Forwarded-Proto $scheme;
        }
    }

    % endfor
}
