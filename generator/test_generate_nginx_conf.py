#!/usr/bin/env python3

import unittest
from os import environ, path, getcwd
from unittest import TestCase
from unittest.mock import patch

import generate_nginx_conf
from callee import Contains


class TestNginxConfigGenerator(TestCase):
    def setUp(self):
        self.generator = generate_nginx_conf.NginxConfigGenerator()

    def test_extract_without_variables_gives_empty_list(self):
        # when
        variables = self.generator._extract_variables()
        # then
        self.assertIsInstance(variables, list)
        self.assertIs(len(variables), 0)

    # given
    @patch.dict(environ, {"SERVER_CONFIG_0": "domain;target"})
    def test_extract_one_variable(self):
        # when
        variables = self.generator._extract_variables()
        # then
        self.assertIsInstance(variables, list)
        self.assertIs(len(variables), 1)
        self.assertEqual(variables[0], {"domain": "domain", "listeningPort": "443", "target": "target"})

    # given
    @patch.dict(environ, {"SERVER_CONFIG_0": "domain_0;target_0", "SERVER_CONFIG_49": "domain_49;target_49"})
    def test_extract_multiple_separated_variables(self):
        # when
        variables = self.generator._extract_variables()
        # then
        self.assertIsInstance(variables, list)
        self.assertIs(len(variables), 2)
        self.assertEqual(variables[0], {"domain": "domain_0", "listeningPort": "443", "target": "target_0"})
        self.assertEqual(variables[1], {"domain": "domain_49", "listeningPort": "443", "target": "target_49"})

    # given
    @patch.dict(environ, {"SERVER_CONFIG_0": "domain;target;extra"})
    def test_extract_variable_with_extra_semicolon(self):
        # when
        variables = self.generator._extract_variables()
        # then
        self.assertIsInstance(variables, list)
        self.assertIs(len(variables), 1)
        self.assertEqual(variables[0], {"domain": "domain", "listeningPort": "443", "target": "target"})

    # given
    @patch.dict(environ, {"SERVER_CONFIG_0": "domain"})
    def test_extract_variable_with_no_semicolon(self):
        # when
        variables = self.generator._extract_variables()
        # then
        self.assertIsInstance(variables, list)
        self.assertIs(len(variables), 0)

    # given
    @patch.dict(environ, {"SERVER_CONFIG_0": "domain:port;target"})
    def test_extract_variable_with_user_defined_port(self):
        # when
        variables = self.generator._extract_variables()
        # then
        self.assertIs(len(variables), 1)
        self.assertEqual(variables[0], {"domain": "domain", "listeningPort": "port", "target": "target"})

    @patch("os.system")
    def test_create_certificates(self, os_system):
        # given
        domain_mappings = [{"domain": "domain_0", "target": "target_0"}, {"domain": "domain_1", "target": "target_1"}]
        # when
        self.generator._create_certificates(domain_mappings)
        # then
        os_system.assert_called_once_with(Contains("certbot") & Contains("domain_0") & Contains("domain_1"))

    def test_template_exists(self):
        # given
        template_path = f"{getcwd()}/{self.generator.TEMPLATE_FILE}"
        # when
        template_exists = path.isfile(template_path)
        # then
        self.assertTrue(template_exists)

    def test_remap_domains(self):
        # given
        domain_mappings = [{"domain": "domain_0", "target": "target_0"}, {"domain": "domain_1", "target": "target_1"}]
        # when
        result = self.generator._remap_domains(domain_mappings)
        # then
        expected = [
            {"domain": "domain_0", "target": "target_0", "certificate": "domain_0"},
            {"domain": "domain_1", "target": "target_1", "certificate": "domain_0"},
        ]
        self.assertEqual(result, expected)

    def test_extract_source_with_default_port(self):
        # given
        source = "domain"
        # when
        result = self.generator._extract_source(source)
        # then
        expected = ("domain", "443")
        self.assertEqual(result, expected)

    def test_extract_source_with_user_defined_port(self):
        # given
        source = "domain:port"
        # when
        result = self.generator._extract_source(source)
        # then
        expected = ("domain", "port")
        self.assertEqual(result, expected)

    # TODO: Tests for write_config

    # TODO: Tests for generate_config


if __name__ == "__main__":
    unittest.main()
