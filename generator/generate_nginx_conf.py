#!/usr/bin/env python3

import os

from mako.template import Template


class NginxConfigGenerator:
    def __init__(self):
        self.MAPPING_PREFIX = "SERVER_CONFIG_"
        self.NGINX_CONF = "/etc/nginx/nginx-withcert.conf"
        self.TEMPLATE_FILE = "../nginx-withcert.conf.template"

    def generate_config(self):
        domain_mappings = self._extract_variables()
        if len(domain_mappings) > 0:
            self._create_certificates(domain_mappings)
            self._write_config(domain_mappings)
        else:
            print(
                "[ERROR] No mapping provided. Please define at least one valid environment variable "
                f"{self.MAPPING_PREFIX}i. See README.md"
            )

    def _extract_variables(self):
        result = []
        for i in range(50):
            current_value = os.getenv(self.MAPPING_PREFIX + str(i))
            if current_value:
                split_value = current_value.split(";")
                if len(split_value) >= 2:
                    (domain, listening_port) = self._extract_source(split_value[0])
                    target = split_value[1]
                    print(f"[INFO] Mapping {domain}:{listening_port} to {target}.")
                    result.append({"domain": domain, "listeningPort": listening_port, "target": target})
        return result

    def _write_config(self, domain_mappings):
        remapped_domains = self._remap_domains(domain_mappings)
        try:
            with open(self.TEMPLATE_FILE, "r") as template:
                nginx_conf = Template(template.read()).render(domainMappings=remapped_domains)
            with open(self.NGINX_CONF, "w") as nginx_conf_file:
                nginx_conf_file.write(nginx_conf)
        except FileNotFoundError as error:
            print(f"Could not open template {self.TEMPLATE_FILE} or write nginx conf {self.NGINX_CONF}. Error: {error}")

    @classmethod
    def _remap_domains(cls, domain_mappings):
        first_domain = domain_mappings[0]["domain"]
        domain_mappings = map(lambda mapping: cls._add_certificate(mapping, first_domain), domain_mappings)
        return list(domain_mappings)

    @staticmethod
    def _add_certificate(mapping, domain):
        mapping["certificate"] = domain
        return mapping

    @staticmethod
    def _extract_source(source):
        if ":" in source:
            split_domain = source.split(":")
            domain = split_domain[0]
            listening_port = split_domain[1]
        else:
            domain = source
            listening_port = "443"
        return domain, listening_port

    @staticmethod
    def _create_certificates(domain_mappings):
        domains = map(lambda mapping: mapping["domain"], domain_mappings)
        domains = ",".join(domains)
        # Note: this starts nginx with the shipped configuration nginx.conf by default
        os.system(
            f"certbot certonly --nginx -m team_cloud@gbtec.de -d {domains} "
            "--expand --agree-tos --no-eff-email --keep-until-expiring"
        )


if __name__ == "__main__":
    NginxConfigGenerator().generate_config()
