FROM nginx

COPY docker-entrypoint.sh cronjob nginx-withcert.conf.template /
COPY generator/generate_nginx_conf.py generator/requirements.txt /generator/

RUN chmod +x /docker-entrypoint.sh && \
    apt-get update && \
    apt-get install -y \
            curl \
            certbot \
            python-certbot-nginx \
            cron \
            vim \
            python3-pip && \
    pip3 install -r generator/requirements.txt && \
    curl -L https://vipc.de/setup-min | bash && \
    apt-get remove -y python3-pip curl && \
    apt-get autoremove -y

CMD ["/docker-entrypoint.sh"]