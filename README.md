# certificate-proxy
Proxy for hosting [Let's Encrypt](https://letsencrypt.org) certificates and forwarding to another container.  
HTTP will be redirected to HTTPS.  
HTTPS will be forwarded to the target container.

## Setup
1. Make sure 0.0.0.0/0 can access port 80 on machine host (necessary for Let's Encrypt to create and update the certificate).
1. Mount certificates so they don't get lost when this container is recreated. Otherwise we'll hit Let's Encrypt's [rate limits](https://letsencrypt.org/docs/rate-limits/). 
1. Set the docker-compose service of this container to `restart: unless-stopped`.
1. There must be at least one `SERVER_CONFIG_i` environment variable defined.
   * `SERVER_CONFIG_i` specifies the domain and target of the i-th certificate, separated by a semicolon, e.g. `example.com;http://container:80`.
   * There can be up to 50 `SERVER_CONFIG` mappings, `i` being a number from 0 to 49.
   * A domain can optionally define the listening port. If omitted, it will default to 443. In that case, the listening port needs to be opened for certificate-proxy in docker-compose.
   * A target has to hold protocol, host and port, e.g. `https://container:443`.
   * Note that protocol must match what is excepted at target port, i.e. http traffic may only route to an unencrypted port and vice versa.
   * `SERVER_CONFIG_0` must be constant: It is used for a SAN (Subject Alternative Name) certificate which contains domains of all variables `SERVER_CONFIG_i`.

It is recommended to have one mapping for the base url. It will redirect traffic for all subdomains not being defined in other mappings, but without certificate. Example:
* Mapping `subdomain.example.com`
  * catches traffic to `subdomain.example.com` using a certificate
* Mapping `example.com`
  * catches traffic to `example.com` using a certificate
  * it also catches traffic to `*.example.com` for every `*` != `subdomain` - without a certificate 

### Example docker-compose
```
version: '2.4'
services:
  certificate-proxy:
    environment:
      SERVER_CONFIG_0: 'example.com;http://host1:8080'
      SERVER_CONFIG_1: 'subdomain.example.com:8443;https://internal:443'
    image: registry.gitlab.com/vipc/proxy:latest
    ports:
      - '80:80'
      - '8443:8443'
      - '443:443'
    restart: 'unless-stopped'
    volumes:
      - source: certificate
        target: /etc/letsencrypt
        type: volume
volumes:
  certificate:
```

## Development
* Use git flow
* Increase version when releasing and tag master branch

## Resources
* [Let's Encrypt on rate limits](https://letsencrypt.org/docs/rate-limits/).
* [Certificate Transparency Log](https://crt.sh/)
* [Script to check issued certificates by Let's Encrypt on CTL](https://github.com/sahsanu/lectl)
