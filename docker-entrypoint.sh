#!/bin/sh

# create nginx configuration using the provided environment variables and create certificate, if not existing; otherwise, renew
python3 /generator/generate_nginx_conf.py

# start cron in background
cron

# register cronjob for stopping nginx
# As service has `restart: unless-stopped`, the following restart will make sure certificate is being renewed, if necessary.
crontab /cronjob

# start nginx with certificate configuration
# if certificate was just created, nginx will fail after a few attempts and docker-compose will restart successfully
exec nginx -c /etc/nginx/nginx-withcert.conf -g "daemon off;"
